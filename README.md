# Gestures
A minimal Gtk+ GUI app for libinput-gestures

<a href='https://flathub.org/apps/details/com.gitlab.cunidev.Gestures'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.png'/></a>

## Video demo:
<a href="https://www.youtube.com/watch?feature=player_embedded&v=3ssSyoFUpcw" target="_blank"><img src="https://img.youtube.com/vi/3ssSyoFUpcw/0.jpg" alt="(click to open video)" width="480" height="360" border="10" /></a>

## Wiki
[The wiki](https://gitlab.com/cunidev/gestures/wikis/home) is still quite small, but it might help, for example, to find useful commands for actions like workspace manipulation and keystroke emulation under Xorg and Wayland.

## Dependencies:
- Python 3 with `gi` module
- meson and ninja
- gettext
- xdotool (recommended)
- libinput-tools
- libinput-gestures

On Debian/Ubuntu, type:
`sudo apt install python3 python3-gi python-gobject meson xdotool libinput-tools gettext`

To install libinput-gestures, follow the instructions on its official page: https://github.com/bulletmark/libinput-gestures

## Install

### Flathub

You can get Gestures directly from Flathub at this link.


<a href='https://flathub.org/apps/details/com.gitlab.cunidev.Gestures'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.png'/></a>

### On Arch Linux
Gestures is also [available in Arch AUR](https://aur.archlinux.org/packages/gestures) (disclaimer: I'm *not* the maintainer).

### On Solus
Solus users can install Gestures directly from the official repos:
Search for *Gestures* in Solus Software Center or install from command line: `sudo eopkg it gestures`

### From source
Please install the above dependencies before proceeding.

You can either build the project inside GNOME Builder (clone repository and hit the Run button) in order to export a Flatpak bundle, or do the following from command line:

```
git clone https://gitlab.com/cunidev/gestures
cd gestures
meson build --prefix=/usr
ninja -C build
sudo ninja -C build install
```

## Bugs

Feel free to report any in the Issues section of GitLab. May take a while for me to fix those, since Gestures is just a small side project I keep for short spans of free time.

## Donations

As for most small open-source projects, the best donation you can give is in time by helping fixing bugs and adding new, shiny features. Contributions and patches (possibly discussed in advance by opening an issue).

However, if you'd really like [to buy me a coffee or a beer](https://paypal.me/tranquillini) as an aid to survive my next exam session, don't forget to thank [libinput-gestures developer bulletmark](https://github.com/bulletmark/) as well, as Gestures is just a GUI built upon their work.
