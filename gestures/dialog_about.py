# About Dialog
#
# Copyright 2021 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, Gdk

from gestures.configfile import ConfigFileHandler
from gestures.gesture import Gesture
from gestures.__version__ import __version__, appid, authors

class AppAboutDialog(Gtk.AboutDialog):
    def __init__(self, parent):
        Gtk.AboutDialog.__init__(self, parent=parent)

        
        self.set_program_name("Gestures")
        self.set_comments("A minimal GTK3 frontend for libinput-gestures")
        self.set_version(__version__)
        
        try:
            self.set_logo(Gtk.IconTheme.get_default().load_icon(appid, 128, 0))
        except:
            pass
        
        self.set_license_type(Gtk.License.GPL_3_0)
        self.set_authors(authors)
        self.set_website("https://gitlab.com/cunidev")
        self.set_website_label("cunidev's GitLab")
        self.set_title("")

        self.connect('response', self.hide_dialog)
        self.connect('delete-event', self.hide_dialog)

    def hide_dialog(self, widget=None, event=None):
        self.hide()
        return True
