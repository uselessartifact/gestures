# Edit Dialog
#
# Copyright 2021 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, Gdk
from copy import deepcopy

from gestures.configfile import ConfigFileHandler
from gestures.gesture import Gesture
from gestures.dialog_error import ErrorDialog
from gestures.__version__ import __version__


class EditDialog(Gtk.Dialog):
    def __init__(self, parent, config_file, i=-1):
        self.lock = False
        self.config_file = config_file

        if (i == -1):
            title = "Add Gesture"
            self.curGesture = Gesture("swipe", "up", "", 2)
        else:
            title = "Edit Gesture"
            self.curGesture = deepcopy(self.config_file.gestures[i])

        Gtk.Dialog.__init__(self, title, parent, 0, Gtk.ButtonsType.NONE)
        self.set_transient_for(parent)
        self.set_modal(True)

        area = self.get_content_area()
        self.grid = Gtk.Grid(margin=10, column_spacing=12)
        self.grid.props.column_homogeneous=True
        area.add(self.grid)
        self.set_default_size(10,10) # default to min size
        self.set_resizable(True)

        label = Gtk.Label(halign=Gtk.Align.START)
        label.set_markup("<b>Type</b>")
        self.grid.add(label)

        self.buttonTypeSwipe = Gtk.RadioButton.new_with_label_from_widget(None, "Swipe")
        self.grid.attach(self.buttonTypeSwipe, 0, 1, 1, 1)

        self.buttonTypePinch = Gtk.RadioButton.new_from_widget(self.buttonTypeSwipe)
        self.buttonTypePinch.set_label("Pinch")
        self.grid.attach(self.buttonTypePinch, 0, 2, 1, 1)

        self.buttonTypeSwipe.set_active((self.curGesture.type != "pinch"))
        self.buttonTypePinch.set_active((self.curGesture.type == "pinch"))


        label = Gtk.Label(halign=Gtk.Align.START)
        label.set_markup("<b>Direction</b>")
        self.grid.attach(label, 1, 0, 1, 1)

        self.buttonDirection1 = Gtk.RadioButton.new_with_label_from_widget(
            None, "Up")
        self.grid.attach(self.buttonDirection1, 1, 1, 1, 1)

        self.buttonDirection2 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection2.set_label("Down")
        self.grid.attach(self.buttonDirection2, 1, 2, 1, 1)

        self.buttonDirection3 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection3.set_label("Left")
        self.grid.attach(self.buttonDirection3, 1, 3, 1, 1)

        self.buttonDirection4 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection4.set_label("Right")
        self.grid.attach(self.buttonDirection4, 1, 4, 1, 1)

        self.buttonDirection5 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection5.set_label("Left Up")
        self.grid.attach(self.buttonDirection5, 1, 5, 1, 1)

        self.buttonDirection6 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection6.set_label("Left Down")
        self.grid.attach(self.buttonDirection6, 1, 6, 1, 1)

        self.buttonDirection7 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection7.set_label("Right Up")
        self.grid.attach(self.buttonDirection7, 1, 7, 1, 1)

        self.buttonDirection8 = Gtk.RadioButton.new_from_widget(
            self.buttonDirection1)
        self.buttonDirection8.set_label("Right Down")
        self.grid.attach(self.buttonDirection8, 1, 8, 1, 1)

        # 1: up/in 2: down/out 3: left/clockwise 4: right/anticlockwise
        self.buttonDirection1.set_active(
            (self.curGesture.direction == "up") or (self.curGesture.direction == "in"))
        self.buttonDirection2.set_active(
            (self.curGesture.direction == "down") or (self.curGesture.direction == "out"))
        self.buttonDirection3.set_active((self.curGesture.direction == "left") or (
            self.curGesture.direction == "clockwise"))
        self.buttonDirection4.set_active((self.curGesture.direction == "right") or (
            self.curGesture.direction == "anticlockwise"))

        label = Gtk.Label(halign=Gtk.Align.START, hexpand=True)
        label.set_markup("<b>Fingers</b>")
        self.grid.attach(label, 2, 0, 1, 1)

        self.buttonFinger2 = Gtk.RadioButton.new_with_label_from_widget(
            None, "Two")
        self.grid.attach(self.buttonFinger2, 2, 1, 1, 1)

        self.buttonFinger3 = Gtk.RadioButton.new_from_widget(
            self.buttonFinger2)
        self.buttonFinger3.set_label("Three")
        self.grid.attach(self.buttonFinger3, 2, 2, 1, 1)

        self.buttonFinger4 = Gtk.RadioButton.new_from_widget(
            self.buttonFinger2)
        self.buttonFinger4.set_label("Four")
        self.grid.attach(self.buttonFinger4, 2, 3, 1, 1)

        if (self.curGesture.fingers != 0):
            self.buttonFinger2.set_active((int(self.curGesture.fingers) == 2))
            self.buttonFinger3.set_active((int(self.curGesture.fingers) == 3))
            self.buttonFinger4.set_active((int(self.curGesture.fingers) == 4))


        suggestions = [
            'xdotool key [code] # simulate keystroke',
            '_internal ws_[direction] # switch workspace',
            'nohup [name] # launch GUI app',
            'notify-send [text] # send notification'
            ]
        for gesture in self.config_file.gestures:
            suggestions.append(gesture.command)

        suggestions = list(set(suggestions)) # remove duplicates

        liststore = Gtk.ListStore(str)
        for s in suggestions:
            liststore.append([s])

        completion = Gtk.EntryCompletion()
        completion.set_model(liststore)
        completion.set_text_column(0)

        self.commandInput = Gtk.Entry(margin_top = 25, margin_left = 5, margin_right = 5)
        self.commandInput.set_completion(completion)
        self.commandInput.set_text(self.curGesture.command)
        self.commandInput.set_hexpand(True)
        self.commandInput.set_placeholder_text("xdotool, _internal, nohup, dbus-send...")
        self.grid.attach(self.commandInput, 0, 9, 3, 1)

        # header bar
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(False)
        hb.props.title = title

        cancelButton = Gtk.Button("Cancel")
        cancelButton.modify_bg(Gtk.StateType.ACTIVE, Gdk.color_parse('red'))
        hb.pack_start(cancelButton)

        confirmButton = Gtk.Button("Confirm")
        Gtk.StyleContext.add_class(confirmButton.get_style_context(), "suggested-action")
        confirmButton.modify_bg(Gtk.StateType.ACTIVE, Gdk.color_parse('teal'))
        hb.pack_end(confirmButton)
        self.set_titlebar(hb)

        # signals - declared at the end to avoid AttributeErrors on not-yet-existing objects

        self.buttonTypeSwipe.connect("toggled", self.onTypeToggle, i, "swipe")
        self.buttonTypePinch.connect("toggled", self.onTypeToggle, i, "pinch")

        self.buttonDirection1.connect("toggled", self.onDirectionToggle, 1)
        self.buttonDirection2.connect("toggled", self.onDirectionToggle, 2)
        self.buttonDirection3.connect("toggled", self.onDirectionToggle, 3)
        self.buttonDirection4.connect("toggled", self.onDirectionToggle, 4)
        self.buttonDirection5.connect("toggled", self.onDirectionToggle, 5)
        self.buttonDirection6.connect("toggled", self.onDirectionToggle, 6)
        self.buttonDirection7.connect("toggled", self.onDirectionToggle, 7)
        self.buttonDirection8.connect("toggled", self.onDirectionToggle, 8)

        self.buttonFinger2.connect("toggled", self.onFingerToggle, 2)
        self.buttonFinger3.connect("toggled", self.onFingerToggle, 3)
        self.buttonFinger4.connect("toggled", self.onFingerToggle, 4)
        self.commandInput.connect("changed", self.onCommandChange)

        confirmButton.connect("clicked", self.onConfirm, i)
        cancelButton.connect("clicked", self.onCancel)
        
        confirmButton.set_can_default(True)
        confirmButton.grab_default()
        # TODO: default not grabbed when other fields are focused
        
        self.show_all()
        
        # SET LABELS - after show_all because of set_visible()
        self.setFingerRadios(self.curGesture.type)
        self.setDirectionLabels(self.curGesture.type)


    def setDirectionLabels(self, type):
        if(type == "pinch"):
            self.buttonDirection1.set_label("In")
            self.buttonDirection2.set_label("Out")
            self.buttonDirection3.set_label("Clockwise")
            self.buttonDirection4.set_label("Anticlockwise")
            self.grid.remove(self.buttonDirection5)
            self.grid.remove(self.buttonDirection6)
            self.grid.remove(self.buttonDirection7)
            self.grid.remove(self.buttonDirection8)
        else:
            self.buttonDirection1.set_label("Up")
            self.buttonDirection2.set_label("Down")
            self.buttonDirection3.set_label("Left")
            self.buttonDirection4.set_label("Right")
            if(self.grid.get_child_at(1,5) == None):
                self.grid.attach(self.buttonDirection5, 1, 5, 1, 1)
                self.grid.attach(self.buttonDirection6, 1, 6, 1, 1)
                self.grid.attach(self.buttonDirection7, 1, 7, 1, 1)
                self.grid.attach(self.buttonDirection8, 1, 8, 1, 1)
        self.resize(10,10) # should shrink to contents

    def onTypeToggle(self, widget, i, type):
        self.curGesture.type = type

        self.setDirectionLabels(type)  # needed after changing type
        self.setFingerRadios(type)
        self.correctDirections()

    def setFingerRadios(self, type):
        if(type == "pinch"):
            self.buttonFinger2.set_sensitive(True)
        else:
            if(self.buttonFinger2.get_active() == True):
                self.buttonFinger3.set_active(True)
                
            self.buttonFinger2.set_sensitive(False)

    def correctDirections(self):
        if(self.buttonDirection2.get_active()):
            direction = 2
        elif(self.buttonDirection3.get_active()):
            direction = 3
        elif(self.buttonDirection4.get_active()):
            direction = 4
        elif(self.buttonDirection5.get_active()):
            direction = 5
        elif(self.buttonDirection6.get_active()):
            direction = 6
        elif(self.buttonDirection7.get_active()):
            direction = 7
        elif(self.buttonDirection8.get_active()):
            direction = 8
        else:
            direction = 1

        self.onDirectionToggle(None, direction)

    def onDirectionToggle(self, widget, direction):
        if(self.curGesture.type == "pinch"):
            if(direction == 2):
                self.curGesture.direction = "out"
            elif(direction == 3):
                self.curGesture.direction = "clockwise"
            elif(direction == 4):
                self.curGesture.direction = "anticlockwise"
            else:
                self.curGesture.direction = "in"  # first case as default
        else:
            if(direction == 2):
                self.curGesture.direction = "down"
            elif(direction == 3):
                self.curGesture.direction = "left"
            elif(direction == 4):
                self.curGesture.direction = "right"
            elif(direction == 5):
                self.curGesture.direction = "left_up"
            elif(direction == 6):
                self.curGesture.direction = "left_down"
            elif(direction == 7):
                self.curGesture.direction = "right_up"
            elif(direction == 8):
                self.curGesture.direction = "right_down"
            else:
                self.curGesture.direction = "up"  # first case as default

    def onFingerToggle(self, widget, finger):
        self.curGesture.fingers = finger

    def onCommandChange(self, widget):
        self.curGesture.command = widget.get_text()

    def onCancel(self, widget):
        self.response(Gtk.ResponseType.CANCEL)

    def onConfirm(self, widget, i):
        if(i != -1):
            self.config_file.gestures[i] = self.curGesture
        else:
            self.config_file.gestures.append(self.curGesture)
        self.config_file.save()
        
        self.response(Gtk.ResponseType.OK)
