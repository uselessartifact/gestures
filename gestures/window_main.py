# Main Window
#
# Copyright 2021 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, Pango

import shlex
from shlex import split
from sys import exit
from subprocess import Popen, call
from os.path import expanduser
from os import getenv
from gestures.configfile import ConfigFileHandler
from gestures.gesture import Gesture
from gestures.__version__ import __version__

from .dialog_preferences import PreferencesDialog, UnsupportedLinesDialog
from .dialog_error import ErrorDialog
from .dialog_edit import EditDialog
from .dialog_about import AppAboutDialog

class MainWindow(Gtk.ApplicationWindow):

    hb = Gtk.HeaderBar()
    btn_alert = Gtk.Button.new_from_icon_name("dialog-warning-symbolic", Gtk.IconSize.BUTTON)

    def __init__(self, app, is_wayland = False):
        self.app = app
        self.is_wayland = is_wayland
        self.editMode = False
        # window
        Gtk.Window.__init__(self, title="Gestures")

        # header bar
        self.hb.set_show_close_button(True)
        self.hb.props.title = "Gestures"

        self.set_titlebar(self.hb)

        # add button
        b = Gtk.Button.new_from_icon_name("list-add-symbolic", Gtk.IconSize.BUTTON)
        b.set_property("tooltip-text", "Add")
        b.connect("clicked", self.on_gesture_add)
        self.hb.pack_start(b)

        # edit button
        b = Gtk.ToggleButton()
        b.set_property("tooltip-text", "Edit")
        icon = Gio.ThemedIcon(name="document-edit-symbolic")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        b.add(image)
        b.connect("clicked", self.on_edit_mode)
        self.hb.pack_start(b)

        # menu button
        b = Gtk.MenuButton(menu_model=self._make_menu(),use_popover=True)
        b.props.image = Gtk.Image.new_from_icon_name("open-menu-symbolic", Gtk.IconSize.BUTTON)
        self.hb.pack_end(b)

        # warning sign
        self.btn_alert.get_style_context().add_class("flat")
        self.btn_alert.set_property("tooltip-text", "Cannot access libinput-gestures")
        self.btn_alert.connect("clicked", self.on_btn_alert)
        self.hb.pack_end(self.btn_alert)

        # contents

        self.box_outer = Gtk.ScrolledWindow()
        self.box_outer.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.add(self.box_outer)
        self.listbox = Gtk.ListBox(margin=0)
        self.listbox.get_style_context().add_class("gesture-list")
        self.listbox.connect("row-activated", self.on_row_activated)
        self.listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.empty = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 20, valign = Gtk.Align.CENTER)
        self.empty.set_visible(True)

        empty_icon = Gtk.Image.new_from_icon_name("touchpad-disabled-symbolic", Gtk.IconSize.DIALOG)
        empty_icon.set_pixel_size(64)
        empty_icon.set_visible(True)
        self.empty.pack_start(empty_icon, False, False, 0)

        empty_label = Gtk.Label()
        empty_label.use_markup = True
        empty_label.set_visible(True)
        empty_label.set_markup("<span foreground=\"gray\" size=\"medium\" weight=\"semibold\">No gestures, mind adding some?</span>")
        self.empty.pack_start(empty_label, False, False, 0)
        self.listbox.set_placeholder(self.empty)
        self.box_outer.add(self.listbox)

    def initialize(self):
        try:
            config_file = ConfigFileHandler(expanduser("~"), __version__)
            if(config_file.generate_config_file()):
                print("INFO: An empty configuration file has been generated")

            config_file.openFile()
            if not(config_file.is_valid()):
                if (config_file.backup()):
                    dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                               "The current configuration file hasn't been created with this tool.")
                    dialog.format_secondary_text("The old file has been backed up to " + config_file.backupPath +
                                                 ", its contents will be extracted and the conf file has been overridden.")
                    dialog.run()
                    dialog.destroy()
                    config_file.save()
                else:
                    dialog = Gtk.MessageDialog(
                        self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Invalid configuration file, can't backup!")
                    dialog.format_secondary_text(
                        "Can't create backup file! For safety reasons, this tool can't be run.")
                    dialog.run()
                    dialog.destroy()
                    exit(-1)
            else:
                pass

        except Exception as e:
            print(e)
            dialog = Gtk.MessageDialog(
                self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Cannot read configuration file.")
            dialog.format_secondary_text(
                "Please check permissions")
            dialog.run()
            dialog.destroy()
            exit(-1)

        # load configuration file
        self.setconfig_file(config_file)
        self.populate(self.is_wayland)

        # and (re)load the service
        self.reload_backend()

    def on_gesture_add(self, widget):
        editDialog = EditDialog(self, self.config_file)
        editDialog.run()
        editDialog.destroy()
        self.populate(self.is_wayland)
        self.reload_backend()

    def on_edit_mode(self, button):
        self.editMode = button.get_active()
        self.populate(self.is_wayland)

    def onEdit(self, widget, i):
        editDialog = EditDialog(self, self.config_file, i)
        editDialog.run()
        editDialog.destroy()
        self.populate(self.is_wayland)
        self.reload_backend()
    
    def on_edit_file(self, a, p):
        dialog = UnsupportedLinesDialog(self, self.config_file)
        dialog.run()
        dialog.destroy()

    def on_edit_file_external(self, a, p):
        self.hide()
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                                   Gtk.ButtonsType.OK_CANCEL, "Opening file in default text editor")
        dialog.format_secondary_text(
            "As Gestures doesn't support real-time file updates yet, external modifications need to be done while Gestures is closed. Would you still like to proceed?")
        dialog.set_modal(True)
        
        if(dialog.run()  == Gtk.ResponseType.OK):
            dialog.destroy()
            call(["xdg-open", self.config_file.filePath])
            exit(0)
        else:
            dialog.destroy()
            self.show()
        
    def on_preferences(self, a, p):
        dialog = PreferencesDialog(self, self.config_file)
        dialog.run()
        dialog.destroy()

    def on_about(self, a, p):
        about_dialog = AppAboutDialog(self)
        about_dialog.run()
        about_dialog.destroy()
        
    def on_row_activated(self, widget, i):
        if(len(self.config_file.gestures) > 0):
            command = self.config_file.gestures[i.get_index()].command
            print("Executing command: " + command)

            try:
                Popen(split(command))
            except:
                # in Flatpak sandbox?
                try:
                    Popen(["flatpak-spawn","--host"] + split(command))
                except:
                    print("Can't execute command: " + command)
            
    def on_import(self, a, p):
        dialog = Gtk.FileChooserNative.new("Import profile", self, Gtk.FileChooserAction.OPEN, None, None)
        
        filter = Gtk.FileFilter() 
        filter.set_name(".conf files") 
        filter.add_pattern("*.conf")
        filter.add_mime_type("text/plain")
        dialog.add_filter(filter)
        
        filter = Gtk.FileFilter()
        filter.set_name("All files") 
        filter.add_pattern("*")
        dialog.add_filter(filter) 
        
        response = dialog.run()
        
        if response == Gtk.ResponseType.ACCEPT:
            path = dialog.get_filename()
            dialog.destroy()
            
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                                   Gtk.ButtonsType.OK_CANCEL, "Overwrite current profile?")
            dialog.format_secondary_text("This operation can't be undone. Make sure you export your current profile first!")
            if(dialog.run() == Gtk.ResponseType.OK):
                if(self.config_file.import_file(path)):
                    dialog.destroy()
                    self.initialize()
                else:
                    dialog.destroy()
                    dialog = Gtk.MessageDialog(
                        self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Can't import profile")
                    dialog.format_secondary_text(
                        "Please check permissions")
                    dialog.set_modal(True)
                    dialog.run()
                    dialog.destroy()
            else:
                    dialog.destroy()
            
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
        
    def on_export(self, a, p):
        dialog = Gtk.FileChooserNative.new("Export profile", self, Gtk.FileChooserAction.SAVE, None, None)

        dialog.set_current_name("Gestures.conf")
        dialog.set_do_overwrite_confirmation(True)
        
        filter = Gtk.FileFilter() 
        filter.set_name(".conf files") 
        filter.add_pattern("*.conf")
        dialog.add_filter(filter)
        
        filter = Gtk.FileFilter()
        filter.set_name("All files") 
        filter.add_pattern("*")
        dialog.add_filter(filter) 
        
        response = dialog.run()
        
        if response == Gtk.ResponseType.ACCEPT:
            path = dialog.get_filename()
            dialog.destroy()

            if not self.config_file.export_file(path):
                dialog = Gtk.MessageDialog(
                    self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Can't export profile")
                dialog.format_secondary_text(
                    "Please check permissions for selected folder")
                dialog.set_modal(True)
                dialog.run()
                dialog.destroy()
            
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
    
    def on_restore(self, a, p):
        self.hide()
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                                   Gtk.ButtonsType.OK_CANCEL, "Restore backup configuration?")
        dialog.format_secondary_text(
            "This operation can't be undone. The app will be closed after restoring, and all changes will be lost.")
        if(dialog.run() == Gtk.ResponseType.OK):
            if(self.config_file.restore()):

                dialog.destroy()
                dialog = Gtk.MessageDialog(
                    self, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.OK, "Backup file restored.")
                dialog.format_secondary_text(
                    "Gestures will be closed. Please remember that re-opening this app might result in overwriting the current backup file.")
                dialog.set_modal(True)
                dialog.run()
                dialog.destroy()
                exit(0)
            else:
                dialog.destroy()
                dialog = Gtk.MessageDialog(
                    self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Can't restore backup file.")
                dialog.format_secondary_text("The file might not exist.")
                dialog.set_modal(True)
                dialog.run()
                dialog.destroy()
                self.show()
        else:
            dialog.destroy()
            self.show()

    def onDelete(self, widget, i):
        dialog = Gtk.MessageDialog(
            self, 0, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK_CANCEL, "Confirm deletion?")
        dialog.format_secondary_text("This operation can't be undone.")
        if(dialog.run() == Gtk.ResponseType.OK):
            del self.config_file.gestures[i]
            self.config_file.save()

            self.reload_backend()

        dialog.destroy()
        self.populate(self.is_wayland)

    def setconfig_file(self, config_file):
        self.config_file = config_file
        
    def populate(self, is_wayland = False):
        # redraw

        for child in self.listbox.get_children():
            child.destroy()

        for i, gesture in enumerate(self.config_file.gestures):
            row = Gtk.ListBoxRow(margin=0)
            row.get_style_context().add_class("gesture-row")
            self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=15)
            row.add(self.hbox)
            
            icon = Gtk.Image.new_from_icon_name("input-touchpad", Gtk.IconSize.DIALOG)
            icon.set_pixel_size(96)
            self.hbox.pack_start(icon, False, False, 10)
            
            vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            vbox.props.valign = Gtk.Align.CENTER

            self.hbox.pack_start(vbox, True, True, 0)

            self.hbox.pack_end(vbox, True, True, 0)

            label = Gtk.Label(xalign=0)
            label.set_markup("<b>" + str(gesture.fingers) + "-finger " + gesture.type + " " + gesture.direction +
                              "</b>")
            vbox.pack_start(label, False, True, 0)
            
            box2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            icon = Gtk.Image.new_from_icon_name("utilities-terminal-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
            label = Gtk.Label("  " + gesture.command, xalign=0)
            label.set_ellipsize(Pango.EllipsizeMode.END)
            box2.add(icon)
            box2.add(label)
            
            vbox.pack_start(box2, False, True, 0)

            if(is_wayland and "xdotool" in gesture.command):
                box2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
                icon = Gtk.Image.new_from_icon_name("dialog-warning", Gtk.IconSize.SMALL_TOOLBAR)
                label = Gtk.Label(xalign=0)
                label.set_markup(" <span color='darkred'><i><small>Unsupported on Wayland sessions.</small></i></span>")
                box2.add(icon)
                box2.add(label)

                vbox.pack_start(box2, False, True, 0)

            if(self.editMode):
                deleteButton = Gtk.Button()
                deleteButton.set_property("tooltip-text", "Delete")
                icon = Gio.ThemedIcon(name="edit-delete-symbolic")
                image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
                deleteButton.add(image)

                editButton = Gtk.Button()
                editButton.set_property("tooltip-text", "Edit")
                icon = Gio.ThemedIcon(name="document-edit-symbolic")
                image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
                editButton.add(image)

                editButton.connect("clicked", self.onEdit, i)

                box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
                box.props.valign = Gtk.Align.CENTER
                Gtk.StyleContext.add_class(box.get_style_context(), "linked")
                
                box.add(editButton)
                box.add(deleteButton)

                deleteButton.connect("clicked", self.onDelete, i)

                self.hbox.pack_start(box, False, True, 10)
            else:
                switch = Gtk.Switch()
                switch.props.active = gesture.enabled
                switch.props.valign = Gtk.Align.CENTER
                self.hbox.pack_start(switch, False, True, 10)
                switch.connect("state-set", self.on_gesture_toggle, i)

            self.listbox.add(row)

        self.listbox.show_all()

    def on_gesture_toggle(self, widget, enabled, i):
        self.config_file.gestures[i].enabled = enabled
        self.config_file.save()
        self.reload_backend()

    def on_btn_alert(self, widget):
        self.reload_backend()

    def reload_backend(self):
        self.btn_alert.hide()
        try:
            self.config_file.reload_process()
        except:
            ErrorDialog(self).backend_not_installed(self)
            self.btn_alert.show()

    def _make_menu(self):
        menu = Gio.Menu.new()
        profile_menu = Gio.Menu.new()
        menu.insert_submenu(0, _("File"), profile_menu)

        profile_menu.append_item(Gio.MenuItem.new(_("Import"), "app.profile_import"))
        profile_menu.append_item(Gio.MenuItem.new(_("Export"), "app.profile_export"))
        profile_menu.append_item(Gio.MenuItem.new(_("Restore backup"), "app.profile_restore"))

        #menu.append_item(Gio.MenuItem.new(_("Edit unsupported"), "app.profile_edit_lines"))
        menu.append_item(Gio.MenuItem.new(_("Edit manually"), "app.profile_edit"))
        menu.append_item(Gio.MenuItem.new(_("Preferences"), "app.preferences"))
        menu.append_item(Gio.MenuItem.new(_("About"), "app.about"))

        a = Gio.SimpleAction.new("preferences", None)
        a.connect("activate", self.on_preferences)
        self.app.add_action(a)
        a = Gio.SimpleAction.new("about", None)
        a.connect("activate", self.on_about)
        self.app.add_action(a)
        a = Gio.SimpleAction.new("profile_edit", None)
        a.connect("activate", self.on_edit_file_external)
        self.app.add_action(a)
        a = Gio.SimpleAction.new("profile_edit_lines", None)
        a.connect("activate", self.on_edit_file)
        self.app.add_action(a)
        a = Gio.SimpleAction.new("profile_import", None)
        a.connect("activate", self.on_import)
        self.app.add_action(a)
        a = Gio.SimpleAction.new("profile_export", None)
        a.connect("activate", self.on_export)
        self.app.add_action(a)
        a = Gio.SimpleAction.new("profile_restore", None)
        a.connect("activate", self.on_restore)
        self.app.add_action(a)

        return menu
