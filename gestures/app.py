# Application entry point
#
# Copyright 2021 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi
import os
import signal
import gettext

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio, Gdk
from .window_main import MainWindow


class Gestures(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='com.gitlab.cunidev.Gestures',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        is_wayland = (os.getenv("XDG_SESSION_TYPE")  == "wayland")
        if(is_wayland):
            print("WARNING: xdotool and wmctrl are not supported by Wayland, meaning that keyboard shortcuts and _internal commands will be ignored.\n\n")

        self._set_css()

        win = MainWindow(self, is_wayland)
        win.set_position(Gtk.WindowPosition.CENTER)

        self.add_window(win)

        win.set_default_size(600, 400)
        win.show_all()

        # populate with meaningful content - could be done (more) automatically
        win.initialize()

    def _set_css(self):
        css = b"""
        .gesture-list {
            padding: 25px;
            background-color: rgb(246,245,244);
        }
        .gesture-row {
            margin: 10px;
            padding: 15px;
            border: 1px #ccc solid;
            border-radius: 8px;
            background-color: white;
        }
        .gesture-row:hover {
            background-color: rgb(250,250,250);
        }
        """ # todo: add proper CSS file
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(css)
        context = Gtk.StyleContext()
        screen = Gdk.Screen.get_default()
        context.add_provider_for_screen(screen, css_provider,
                                    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)



def main(version):
    app = Gestures()
    return app.run(sys.argv)
